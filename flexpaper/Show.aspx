﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="flexpaper.Show" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/flexpaper.js"></script>
    <script type="text/javascript" src="js/flexpaper_handlers.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="displayfile" runat="server" Value="" />
        <div id="documentViewer" style="width: 100%; height: 100%; position: fixed;"></div>
        <script type="text/javascript">
            $('#documentViewer').FlexPaperViewer(
              {
                  config: {
                      SwfFile: escape($("#displayfile").val()),
                      Scale: 0.6,
                      ZoomTransition: 'easeOut',
                      ZoomTime: 0.5,
                      ZoomInterval: 0.2,
                      FitPageOnLoad: true,
                      FitWidthOnLoad: true,
                      FullScreenAsMaxWindow: false,
                      ProgressiveLoading: false,
                      MinZoomSize: 0.2,
                      MaxZoomSize: 5,
                      SearchMatchAll: false,
                      InitViewMode: 'Portrait',

                      ViewModeToolsVisible: false,
                      ZoomToolsVisible: true,
                      NavToolsVisible: true,
                      CursorToolsVisible: false,
                      SearchToolsVisible: true,

                      jsDirectory: 'js/',

                      localeChain: 'en_US'
                  }
              });
        </script>
    </form>
</body>
</html>
